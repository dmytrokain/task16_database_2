select maker from Product
where exists (select maker 
from Product 
where model in (select model
from Printer
where model in (select model 
from PC 
where speed > 750)));