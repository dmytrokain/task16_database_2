select maker, type from Product
where exists (select model
from PC
where Product.model=model and
price in (select max(price)
from PC) or (select model
from Printer
where Product.model=model));