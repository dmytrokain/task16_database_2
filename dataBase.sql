create database University;
use University;
create table Student
(   id          int AUTO_INCREMENT PRIMARY KEY,
    name        char(20)NULL,
    surname     char(20)NULL,
    date_birth  varchar(50),
    adress      varchar(50),
    subject_code    int NULL,
    pecialty_code  int NULL
);

create table Subject 
(
    id          int AUTO_INCREMENT PRIMARY KEY,
    name        varchar(50),
    teacher     varchar(50),
    semester    int
);

create table Specialty 
(
    id          int AUTO_INCREMENT PRIMARY KEY,
    name        varchar(50)
);


ALTER TABLE Student ADD CONSTRAINT FK_Students_Subject FOREIGN KEY (subject_code) REFERENCES Subject(id);
ALTER TABLE Student ADD CONSTRAINT FK_Students_Specialty FOREIGN KEY (pecialty_code) REFERENCES Specialty(id);
